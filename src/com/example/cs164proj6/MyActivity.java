package com.example.cs164proj6;

import android.animation.*;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;

import java.util.ArrayList;

public class MyActivity extends Activity {
    public static int ANIMATION_LENGTH = 700;
    boolean animationIsRunning = false;
    View square1;
    View square2;
    View square3;
    Button button;
    AnimatorSet masterAnimatorSet;

    /**
     * Coordinates is a data structure used to encapsulate the x,y-coordinates of a point.
     */
    private class Coordinates {
        int x;
        int y;
        public Coordinates(int x, int y) {
            this.x = x;
            this.y = y;
        }
    }
    ArrayList<Coordinates> coordinates;


    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        square1 = findViewById(R.id.square1);
        square2 = findViewById(R.id.square2);
        square3 = findViewById(R.id.square3);
        coordinates = new ArrayList<Coordinates>();
        coordinates.add(new Coordinates(square1.getLeft(), square1.getTop()));
        coordinates.add(new Coordinates(square2.getLeft(), square2.getTop()));
        coordinates.add(new Coordinates(square3.getLeft(), square3.getTop()));
        button = (Button)findViewById(R.id.button);
        button.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_dark));
        masterAnimatorSet = new AnimatorSet();
        initAnimations();
    }

    /**
     * initAnimations() is a function which creates all of the animations used in the application.  This function does
     * not start the animations.
     */
    private void initAnimations() {
        // Create the fade-in animation for square1.  When done, set the square's visibility.
        ObjectAnimator fadein1 = ObjectAnimator.ofFloat(square1, "alpha", 0.0f, 1.0f);
        fadein1.setDuration(ANIMATION_LENGTH);
        fadein1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                square1.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {}

            @Override
            public void onAnimationCancel(Animator animation) {}

            @Override
            public void onAnimationRepeat(Animator animation) {}
        });

        // Create the fade-in animation for square2.  When done, set the square's visibility.  Copy-pasta...
        ObjectAnimator fadein2 = ObjectAnimator.ofFloat(square2, "alpha", 0.0f, 1.0f);
        fadein2.setDuration(ANIMATION_LENGTH);
        fadein2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                square2.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {}

            @Override
            public void onAnimationCancel(Animator animation) {}

            @Override
            public void onAnimationRepeat(Animator animation) {}
        });

        // Create the fade-in animation for square3.  When done, set the square's visibility.
        ObjectAnimator fadein3 = ObjectAnimator.ofFloat(square3, "alpha", 0.0f, 1.0f);
        fadein3.setDuration(ANIMATION_LENGTH);
        fadein3.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
                square3.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animator animation) {
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });

        // Create the translation animation for squares 1 and 2, where they translate together.
        ValueAnimator moveXY12 = ValueAnimator.ofFloat(0, 100);
        moveXY12.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // As the square animates, make sure the object's values are also updated.
                float value = (Float)animation.getAnimatedValue();
                ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) square1.getLayoutParams();
                mlp.leftMargin += 1;
                mlp.topMargin += 1;
                square1.setLayoutParams(mlp);

                // Same for square 2: make sure the object's values are updated as the square animates.
                ViewGroup.MarginLayoutParams mlp2 = (ViewGroup.MarginLayoutParams) square2.getLayoutParams();
                mlp.leftMargin += 1;
                mlp.topMargin += 1;
                square2.setLayoutParams(mlp2);
            }
        });

        // Create the translation animation for square3.  This translation is different from that of square1 and square2.
        ValueAnimator moveXY3 = ValueAnimator.ofFloat(0, 30);
        moveXY3.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                // As the square animates, make sure the object's values are also updated.
                float value = (Float)animation.getAnimatedValue();
                ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) square3.getLayoutParams();
                mlp.leftMargin += (int)1;
                mlp.topMargin -= (int)1;
                square3.setLayoutParams(mlp);
            }
        });

        // Set up the fade-out code for square1.  Make sure to update visibility appropriately afterwards.
        ObjectAnimator fadeout1 = ObjectAnimator.ofFloat(square1, "alpha", 1.0f, 0.0f);
        fadeout1.setDuration(ANIMATION_LENGTH);
        fadeout1.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                square1.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });

        // Set up the fade-out code for square2.  Make sure to update visibility appropriately afterwards.
        ObjectAnimator fadeout2 = ObjectAnimator.ofFloat(square2, "alpha", 1.0f, 0.0f);
        fadeout2.setDuration(ANIMATION_LENGTH);
        fadeout2.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                square2.setVisibility(View.INVISIBLE);

            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });

        // Set up the fade-out code for square3.  Make sure to update visibility appropriately afterwards.
        ObjectAnimator fadeout3 = ObjectAnimator.ofFloat(square3, "alpha", 1.0f, 0.0f);
        fadeout3.setDuration(ANIMATION_LENGTH);
        fadeout3.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {
            }

            @Override
            public void onAnimationEnd(Animator animation) {
                square3.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onAnimationCancel(Animator animation) {
            }

            @Override
            public void onAnimationRepeat(Animator animation) {
            }
        });

        // Create an AnimatorSet to handle the fading of square1 and square2 together.
        AnimatorSet fadeIn12 = new AnimatorSet();
        fadeIn12.playTogether(fadein1, fadein2);

        // Have the squares rotate sequentially, one after another.
        masterAnimatorSet.playSequentially(fadeIn12, fadein3, moveXY12, moveXY3,
                ObjectAnimator.ofFloat(square1, "rotation", 0f, 45f),
                ObjectAnimator.ofFloat(square2, "rotation", 0f, 95f),
                ObjectAnimator.ofFloat(square3, "rotation", 0f, 155f),
                fadeout3, fadeout2, fadeout1);

        masterAnimatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                super.onAnimationEnd(animation);
                changeToStartAnimationButton();
            }
        });
    }

    /**
     * changeToStartAnimationButton() is a helper function which toggles the state of our button to start animation.
     */
    private void changeToStartAnimationButton() {
        button.setBackgroundColor(getResources().getColor(android.R.color.holo_blue_dark));
        button.setText("Start animation");
    }

    /**
     * changeToPauseAnimationButton() is a helper function which toggles the state of our button to reset animation.
     */
    private void changeToPauseAnimationButton() {
        button.setBackgroundColor(getResources().getColor(android.R.color.holo_green_dark));
        button.setText("Pause animation");
    }

    /**
     * Response handler to react to taps on our button which toggles animations.
     * @param v is the view which registered the tap
     */
    public void onClick(View v) {
        // Case 1: Animations are currently running.  Hitting the button will reset the animation.
        if (masterAnimatorSet.isRunning()) {
            masterAnimatorSet.cancel();

            // Toggle button status.
            changeToStartAnimationButton();

            // Set up the animations to translate square1 back to its starting position.  (x-coordinate)
            ValueAnimator moveBackX1 = ValueAnimator.ofFloat(square1.getLeft(), coordinates.get(0).x);
            moveBackX1.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    float value = (Float)animation.getAnimatedValue();
                    ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) square1.getLayoutParams();
                    mlp.leftMargin = (int)value;
                    square1.setLayoutParams(mlp);
                }
            });
            moveBackX1.start();

            // Set up the animations to translate square1 back to its starting position.  (y-coordinate)
            ValueAnimator moveBackY1 = ValueAnimator.ofFloat(square1.getTop(), coordinates.get(0).y);
            moveBackY1.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    float value = (Float)animation.getAnimatedValue();
                    ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) square1.getLayoutParams();
                    mlp.topMargin = (int)value;
                    square1.setLayoutParams(mlp);
                }
            });
            moveBackY1.start();

            // Rotate square1 back to its starting position.
            ObjectAnimator.ofFloat(square1, "rotation", square1.getRotation(), 0f).start();
            // TODO: Dangerous, needs to set VISIBILITY. change later

            // Fade square1 back to its original alpha value.
            ObjectAnimator.ofFloat(square1, "alpha", square1.getAlpha(), 0f).start();

            // Set up the animations to translate square2 back to its starting position. (x-coordinate)
            ValueAnimator moveBackX2 = ValueAnimator.ofFloat(square2.getLeft(), coordinates.get(0).x);
            moveBackX2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    float value = (Float)animation.getAnimatedValue();
                    ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) square2.getLayoutParams();
                    mlp.leftMargin = (int)value;
                    square2.setLayoutParams(mlp);
                }
            });
            moveBackX2.start();

            // Set up the animations to translate square2 back to its starting position. (y-coordinate)
            ValueAnimator moveBackY2 = ValueAnimator.ofFloat(square2.getTop(), coordinates.get(0).y);
            moveBackY2.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    float value = (Float)animation.getAnimatedValue();
                    ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) square2.getLayoutParams();
                    mlp.topMargin = (int)value;
                    square2.setLayoutParams(mlp);
                }
            });
            moveBackY2.start();

            // Rotate square2 back to its starting position.
            ObjectAnimator.ofFloat(square2, "rotation", square2.getRotation(), 0f).start();
            // TODO: Dangerous, needs to set VISIBILITY. change later

            // Reset square2 back to its starting alpha.
            ObjectAnimator.ofFloat(square2, "alpha", square2.getAlpha(), 0f).start();

            // Set up the animations to translate square3 back to its starting position. (x-coordinate)
            ValueAnimator moveBackX3 = ValueAnimator.ofFloat(square3.getLeft(), coordinates.get(0).x);
            moveBackX3.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    float value = (Float)animation.getAnimatedValue();
                    ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) square3.getLayoutParams();
                    mlp.leftMargin = (int)value;
                    square3.setLayoutParams(mlp);
                }
            });
            moveBackX3.start();

            // Set up the animations to translate square3 back to its starting position. (y-coordinate)
            ValueAnimator moveBackY3 = ValueAnimator.ofFloat(square3.getTop(), coordinates.get(0).y);
            moveBackY3.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(ValueAnimator animation) {
                    float value = (Float)animation.getAnimatedValue();
                    ViewGroup.MarginLayoutParams mlp = (ViewGroup.MarginLayoutParams) square3.getLayoutParams();
                    mlp.topMargin = (int)value;
                    square3.setLayoutParams(mlp);
                }
            });
            moveBackY3.start();

            // Rotate square3 back to its starting position.
            ObjectAnimator.ofFloat(square3, "rotation", square3.getRotation(), 0f).start();
            // TODO: Dangerous, needs to set VISIBILITY. change later

            // Reset square2 back to its starting alpha.
            ObjectAnimator.ofFloat(square3, "alpha", square3.getAlpha(), 0f).start();
        } else {
            // Case 2: Animation is not running right now, so start the animations and adjust the button as appropriate.
            masterAnimatorSet.start();
            changeToPauseAnimationButton();
        }
    }
}
